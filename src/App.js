import React from 'react';
import './App.css';

function GakusciText(){
  return (
    <p>Gakusci React POC <br/>
      <strong>This application is for prototyping purpose <br/>
      /!\ DO NOT USE IT IN PRODUCTION /!\</strong>
    </p>
  );
}

class ResearchTextArea extends React.Component {

  retrieveResult() {
    console.log("GET RESULT");
  }

  async search() {
    console.log("SEARCH");
    const queryValue = document.getElementById('query').value;
    const response = await fetch('http://localhost:8080/search/?q=' + queryValue);
    const myJson = await response.json();
    console.log("GET RESULT: \n");
    console.log(JSON.stringify(myJson));
  }

  render() {
    return (
      <div>
      <form>
        <input id="query" type="text" name="q"/>
      </form>
      <button id="search" onClick={this.search}>Search</button>
      </div>
    );
  }
}

function ResearchBar() {
  return (
    <div>
        <ResearchTextArea/>
    </div>
  );
}


function App() {
  return (
    <div className="App">
    <GakusciText/>
    <ResearchBar/>
    </div>
  );
}

export default App;
